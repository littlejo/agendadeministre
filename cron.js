const moment = require('moment');
const fs = require('fs');

var scrap = {};
scrap['philippe-edouard'] = require('./scrap/philippe-edouard.js');
scrap['o-cedric'] = require('./scrap/o-cedric.js');
scrap['le-maire-bruno'] = require('./scrap/le-maire-bruno.js');
scrap['dussopt-olivier'] = require('./scrap/dussopt-olivier.js');
scrap['darmanin-gerald'] = require('./scrap/darmanin-gerald.js');
scrap['pannier-runacher-agnes'] = require('./scrap/pannier-runacher-agnes.js');
scrap['blanquer-jean-michel'] = require('./scrap/blanquer-jean-michel.js');
scrap['vidal-frederique'] = require('./scrap/vidal-frederique.js');
scrap['attal-gabriel'] = require('./scrap/attal-gabriel.js');
scrap['riester-franck'] = require('./scrap/riester-franck.js');

var cron = function () {
	if (global.env == "dev") {
		console.log("Running cron...");
	}
	fs.writeFile('data/cron.txt', moment().format(),
		function (err) {
			if (err) {
				return console.log(err);
			}
	});
	
	for (s in scrap) {
		scrap[s].scrap();
	}
}

cron();

exports.execute = cron;