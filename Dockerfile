FROM node:lts-alpine3.11

ADD package.json .
RUN npm install

WORKDIR /app
ADD . /app/

CMD ["npm", "start"]
