# AgendaDeMinistre

> Vous avez toujours rêvé d'avoir un agenda de ministre ?

## Installation

Clonez :
```
git clone https://framagit.org/DavidLibeau/agendadeministre.git
```

Installez :
```
npm install
```

Il vous faudra peut-être installer browserify.
```
npm install -g browserify
```

Lancez le serveur :
```
npm start
```

Et voilà, le serveur est démarré sur `http://localhost:3030` !



## Contribuer

Toutes contributions sont les bienvenues ! Par exemple, vous pouvez coder des scrappeur.

Voici la liste des agendas des membres du gouvernement à scrapper :

* [ ]  M. Emmanuel MARCON
* [X]  M. Edouard PHILIPPE
* [ ]  M. Christophe CASTANER
* [ ]  Mme Nicole BELLOUBET
* [X]  M. Gérald DARMANIN
* [ ]  Mme Florence PARLY
* [X]  M. Bruno LE MAIRE
* [ ]  Mme Muriel PÉNICAUD
* [ ]  M. Franck RIESTER
* [ ]  Mme Frédérique VIDAL
* [ ]  M. Jean-Michel BLANQUER
* [ ]  M. Jean-Yves LE DRIAN
* [ ]  M. Julien DENORMANDIE
* [ ]  M. Didier GUILLAUME
* [ ]  Mme Elisabeth BORNE
* [ ]  M. Olivier VÉRAN
* [ ]  Mme Jacqueline GOURAULT
* [ ]  Mme Roxana MARACINEANU
* [ ]  Mme Annick GIRARDIN
* [ ]  M. Sébastien LECORNU
* [ ]  M. Marc FESNEAU
* [ ]  M. Adrien TAQUET
* [X]  Mme Agnès PANNIER-RUNACHER
* [ ]  Mme Amélie DE MONTCHALIN
* [ ]  Mme Brune POIRSON
* [X]  M. Cédric O
* [ ]  Mme Christelle DUBOS
* [ ]  Mme Emmanuelle WARGON
* [ ]  M. Gabriel ATTAL
* [ ]  Mme Geneviève DARRIEUSSECQ
* [ ]  M. Jean-Baptiste DJEBBARI
* [ ]  M. Jean-Baptiste LEMOYNE
* [ ]  M. Laurent NUNEZ
* [ ]  M. Laurent PIETRASZEWSKI
* [ ]  Mme Marlène SCHIAPPA
* [X]  M. Olivier DUSSOPT
* [ ]  Mme Sophie CLUZEL
* [ ]  Mme Sibeth NDIAYE

