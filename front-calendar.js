var $ = require('jquery');
require('tui-dom');
require('tui-code-snippet');
require('tui-date-picker');
require('tui-time-picker');
var Calendar = require('tui-calendar');
var moment = require('moment');

var MONTHLY_CUSTOM_THEME = {
	// month header 'dayname'
	'month.dayname.height': '42px',
	'month.dayname.borderLeft': 'none',
	'month.dayname.paddingLeft': '8px',
	'month.dayname.paddingRight': '0',
	'month.dayname.fontSize': '13px',
	'month.dayname.backgroundColor': 'inherit',
	'month.dayname.fontWeight': 'normal',
	'month.dayname.textAlign': 'left',

	// month day grid cell 'day'
	'month.holidayExceptThisMonth.color': '#f3acac',
	'month.dayExceptThisMonth.color': '#bbb',
	'month.weekend.backgroundColor': '#fafafa',
	'month.day.fontSize': '16px',

	// month schedule style
	'month.schedule.borderRadius': '5px',
	'month.schedule.height': '18px',
	'month.schedule.marginTop': '2px',
	'month.schedule.marginLeft': '10px',
	'month.schedule.marginRight': '10px',

	// month more view
	'month.moreView.boxShadow': 'none',
	'month.moreView.paddingBottom': '0',
	'month.moreView.border': '1px solid #9a935a',
	'month.moreView.backgroundColor': '#f9f3c6',
	'month.moreViewTitle.height': '28px',
	'month.moreViewTitle.marginBottom': '0',
	'month.moreViewTitle.backgroundColor': '#f4f4f4',
	'month.moreViewTitle.borderBottom': '1px solid #ddd',
	'month.moreViewTitle.padding': '0 10px',
	'month.moreViewList.padding': '10px'
};

var templates = {
	popupIsAllDay: function () {
		return 'Toute la journée';
	},
	popupStateFree: function () {
		return 'Libre';
	},
	popupStateBusy: function () {
		return 'Occupé';
	},
	titlePlaceholder: function () {
		return 'Sujet';
	},
	locationPlaceholder: function () {
		return 'Lieu';
	},
	startDatePlaceholder: function () {
		return 'Date de début';
	},
	endDatePlaceholder: function () {
		return 'Date de fin';
	},
	popupSave: function () {
		return 'Sauvegarder';
	},
	popupUpdate: function () {
		return 'Mettre à jour';
	},
	popupDetailDate: function (isAllDay, start, end) {
		var isSameDate = moment(start).isSame(end);
		var endFormat = (isSameDate ? '' : 'YYYY.MM.DD ') + 'hh:mm a';

		if (isAllDay) {
			return moment(start).format('YYYY.MM.DD') + (isSameDate ? '' : ' - ' + moment(end).format('YYYY.MM.DD'));
		}

		return (moment(start).format('YYYY.MM.DD hh:mm a') + ' - ' + moment(end).format(endFormat));
	},
	popupDetailLocation: function (schedule) {
		return 'Lieu : ' + schedule.location;
	},
	popupDetailUser: function (schedule) {
		if (schedule.attendees.length > 1) {
			return 'Participant·e·s : ' + (schedule.attendees || []).join(', ');
		} else {
			return 'Participant·e : ' + (schedule.attendees || []).join(', ');
		}
	},
	popupDetailState: function (schedule) {
		return 'Etat : ' + schedule.state || 'Busy';
	},
	popupDetailRepeat: function (schedule) {
		return 'Répétition : ' + schedule.recurrenceRule;
	},
	popupDetailBody: function (schedule) {
		return 'Corps : ' + schedule.body;
	},
	popupEdit: function () {
		return 'Editer';
	},
	popupDelete: function () {
		return 'Supprimer';
	}
};

var calendar = new Calendar('#calendar', {
	defaultView: 'week',
	taskView: false,
	template: templates,
	theme: MONTHLY_CUSTOM_THEME,
	month: {
		daynames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		startDayOfWeek: 1,
		narrowWeekend: false
	},
	week: {
		daynames: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
		startDayOfWeek: 1,
		narrowWeekend: false
	},
	usageStatistics: false,
	useDetailPopup: true,
	isReadOnly: true
});

var thisCalendarData = [];
var calendarReceived = 0;
for (c in calendarFiles) {
	$.ajax(calendarFiles[c])
		.done(function (data) {
			console.log(data)
			thisCalendarData = thisCalendarData.concat(data);
			calendarReceived++;
		});
}

var waitForUpdate = setInterval(function () {
	if (calendarReceived >= calendarFiles.length) {
		calendar.createSchedules(thisCalendarData);
		console.log("Update calendar with " + calendarReceived + " calendar(s) and " + thisCalendarData.length + " event(s).");
		clearInterval(waitForUpdate);
	}
}, 100);



$("#changeView").click(function () {
	var view = calendar.getViewName();
	if (view == 'month') {
		calendar.changeView('week');
		$(this).html('<i class="fa fa-calendar-minus-o" aria-hidden="true"></i> Vue mois');

	} else if (view == 'week') {
		calendar.changeView('month');
		$(this).html('<i class="fa fa-calendar-plus-o" aria-hidden="true"></i> Vue semaine');

	}
})