var fetch = require('node-fetch');
var fs = require('fs');

var nSave = async function (url, filename = undefined) {
	if (filename == undefined) {
		filename = url.split('/').pop();
	}
	try {
		if (global.env == "dev") {
			console.log("fetch(" + url + ")");
		}
		const res = await fetch(url);

		if (res.status >= 400) {
			throw new Error("Bad response from server (status code : "+res.status+" for url "+url+")");
		}

		var data = await res.text();
		fs.writeFile('archives/' + filename, JSON.stringify(data),
			function (err) {
				if (err) {
					return console.log(err);
				}

			});

		return new Promise(resolve => {
			resolve(data)
		})
	} catch (err) {
		console.error(err);
	}
}

exports.nSave = nSave;