var openAgendaScrapper = require('./scrap-OpenAgenda');

var calendarId = 'le-maire-bruno';
var openAgendaId = '41488194';

var scrap = async function () {
	openAgendaScrapper.scrap(calendarId, openAgendaId);
}

exports.scrap = scrap;