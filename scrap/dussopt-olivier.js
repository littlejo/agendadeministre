var openAgendaScrapper = require('./scrap-OpenAgenda');

var calendarId = 'dussopt-olivier';
var openAgendaId = '91242531';

var scrap = async function () {
	openAgendaScrapper.scrap(calendarId, openAgendaId);
}

exports.scrap = scrap;