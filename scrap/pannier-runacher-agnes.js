var openAgendaScrapper = require('./scrap-OpenAgenda');

var calendarId = 'pannier-runacher-agnes';
var openAgendaId = '31602919';

var scrap = async function () {
	openAgendaScrapper.scrap(calendarId, openAgendaId);
}

exports.scrap = scrap;