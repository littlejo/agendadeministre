var openAgendaScrapper = require('./scrap-OpenAgenda');

var calendarId = 'o-cedric';
var openAgendaId = '3492148';

var scrap = async function () {
	openAgendaScrapper.scrap(calendarId, openAgendaId);
}

exports.scrap = scrap;